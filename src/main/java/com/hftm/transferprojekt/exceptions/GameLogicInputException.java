package com.hftm.transferprojekt.exceptions;

public class GameLogicInputException extends Exception {
    public GameLogicInputException(){}
    public GameLogicInputException(String message){
        super(message);
    }
}
