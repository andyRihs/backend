package com.hftm.transferprojekt.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Error on winnerResultsNotAccepted")
public class WinnerResultsNotAcceptedException extends RuntimeException {
    public WinnerResultsNotAcceptedException() {
        super();
    }
    public WinnerResultsNotAcceptedException(String message, Throwable cause) {
        super(message, cause);
    }
    public WinnerResultsNotAcceptedException(String message) {
        super(message);
    }
    public WinnerResultsNotAcceptedException(Throwable cause) {
        super(cause);
    }
}
