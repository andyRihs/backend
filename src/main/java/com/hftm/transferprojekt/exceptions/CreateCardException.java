package com.hftm.transferprojekt.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

//remove: reason="Error on creating card"
@ResponseStatus(value= HttpStatus.NOT_FOUND)
public class CreateCardException extends RuntimeException {
    public CreateCardException() {
        super();
    }
    public CreateCardException(String message, Throwable cause) {
        super(message, cause);
    }
    public CreateCardException(String message) {
        super(message);
    }
    public CreateCardException(Throwable cause) {
        super(cause);
    }
}
