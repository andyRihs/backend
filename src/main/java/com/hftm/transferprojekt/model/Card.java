package com.hftm.transferprojekt.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class Card{
    @Id
    @Column(name = "CARD_ID", updatable = false, nullable = false)
    private String id;
    private boolean accept;

    @OneToMany(cascade = CascadeType.ALL)
    private List<BuzzWord> buzzWords;

    public Card() {
        this.id = UUID.randomUUID().toString();
        this.buzzWords = new ArrayList();
        this.accept = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isAccept() {
        return accept;
    }

    public void setAccept(boolean accept) {
        this.accept = accept;
    }

    public List<BuzzWord> getBuzzWords() {
        return buzzWords;
    }

    public void setBuzzWords(List<BuzzWord> buzzWords) {
        this.buzzWords = buzzWords;
    }
}
