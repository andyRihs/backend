package com.hftm.transferprojekt.model;

public enum Level {
    EASY(1),
    MEDIUM(2),
    HEAVY(3);

    // declaring private variable for getting values
    private int numericValue;

    // getter method
    public int getNumericValue()
    {
        return this.numericValue;
    }

    // enum constructor - cannot be public or protected
    private Level(int numericValue)
    {
        this.numericValue = numericValue;
    }
}
