package com.hftm.transferprojekt.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
public class Game{
    @Id
    @Column(name = "GAME_ID", updatable = false, nullable = false)
    private String id;

    @NotNull
    private int duration;

    @NotNull
    private boolean needFullCard;

    @NotNull
    private String gameName;

    @NotNull
    private Date startDate;

    @NotNull
    private Level level;

    @OneToOne(cascade = {CascadeType.ALL})
    private Player gameMaster;

    @NotNull
    private String gameCode;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Player> players;

    private  String winnerNickName;

    protected Game() {
        // no-args constructor required by JPA spec
        // this one is protected since it shouldn't be used directly
    }

    public Game(int duration, boolean fullCard, String gameName, Date startDate, Level level, Player gameMaster) {
        this.id = UUID.randomUUID().toString();
        this.duration = duration;
        this.needFullCard = fullCard;
        this.gameName = gameName;
        this.startDate = startDate;
        this.level = level;
        this.players = new ArrayList<Player>();
        this.players.add(gameMaster);
        this.gameMaster = gameMaster;
        this.winnerNickName = null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public boolean isNeedFullCard() {
        return needFullCard;
    }

    public void setNeedFullCard(boolean fullCard) {
        this.needFullCard = fullCard;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public Player getGameMaster() {
        return gameMaster;
    }

    public void setGameMaster(Player gameMaster) {
        this.gameMaster = gameMaster;
    }

    public String getGameCode() {
        return gameCode;
    }

    public void setGameCode(String gameCode) {
        this.gameCode = gameCode;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public String getWinnerNickName() {
        return winnerNickName;
    }

    public void setWinnerNickName(String winnerNickName) {
        this.winnerNickName = winnerNickName;
    }
}
