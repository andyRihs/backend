package com.hftm.transferprojekt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
public class BuzzWord{
    @Id
    @Column(name = "BUZZWORD_ID", updatable = false, nullable = false)
    private String id;
    @NotNull
    private String buzzWord;
    @NotNull
    private int counter;
    @NotNull
    private int xPosition;
    @NotNull
    private int yPosition;

    //need in Springboot
    public BuzzWord() {
        this.id = UUID.randomUUID().toString();
        // no-args constructor required by JPA spec
    }

    public BuzzWord(String buzzWord, int xPosition, int yPosition) {
        this.id = UUID.randomUUID().toString();
        this.buzzWord = buzzWord;
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.counter = 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuzzWord() {
        return buzzWord;
    }

    public void setBuzzWord(String buzzWord) {
        this.buzzWord = buzzWord;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public int getxPosition() { return xPosition; }

    public void setxPosition(int xPosition) { this.xPosition = xPosition; }

    public int getyPosition() { return yPosition; }

    public void setyPosition(int yPosition) { this.yPosition = yPosition; }
}

