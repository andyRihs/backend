package com.hftm.transferprojekt.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.UUID;

@Entity
public class Player{
    @Id
    @Column(name = "PLAYER_ID", updatable = false, nullable = false)
    private String id;

    @NotNull
    private String nickName;

    @Null
    private String session;

    @ManyToOne
    private Game game;

    @OneToOne(cascade = {CascadeType.ALL})
    private Card card;

    protected Player() {
        // no-args constructor required by JPA spec
        // this one is protected since it shouldn't be used directly
    }

    // Constructor for GameMaster
    public Player(String nickName) {
        this.id = UUID.randomUUID().toString();
        this.nickName = nickName;
    }

    // Constructor for Players - ist das noch nötig ohne WebSockets? Nelson/Andy
    public Player(String nickName, String session) {
        this.id = UUID.randomUUID().toString();
        this.nickName = nickName;
        this.session = session;
    }

    // Constructor for Players
    public Player(String nickName, Game game) {
        this.id = UUID.randomUUID().toString();
        this.nickName = nickName;
        this.game = game;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
}
