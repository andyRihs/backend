package com.hftm.transferprojekt.controller.model;

public class WinnerResultsRequest {
    private final String gameCode;

    public WinnerResultsRequest(String gameCode) {
        this.gameCode = gameCode;
    }

    public String getGameCode() {
        return gameCode;
    }
}
