package com.hftm.transferprojekt.controller.model;

public class CreateCardResponse {
    private final String gameCode;

    public CreateCardResponse(String gameCode) {
        this.gameCode = gameCode;
    }

    public String getGameCode() {
        return gameCode;
    }
}
