package com.hftm.transferprojekt.controller.model;

public class WinnerResultsNotAcceptedRequest {
    private final String gameCode;
    private final String nickName;

    public WinnerResultsNotAcceptedRequest(String gameCode, String gameName) {
        this.gameCode = gameCode;
        this.nickName = gameName;
    }

    public String getGameCode() {
        return gameCode;
    }

    public String getNickName() {
        return nickName;
    }
}
