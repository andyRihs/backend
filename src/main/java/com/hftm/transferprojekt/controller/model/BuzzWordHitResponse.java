package com.hftm.transferprojekt.controller.model;

import com.hftm.transferprojekt.model.BuzzWord;

public class BuzzWordHitResponse {
    private final String gameCode;
    private final BuzzWord buzzword;
    private final String nickName;
    private boolean isWinner;
    private String winnerNickName;

    public BuzzWordHitResponse(String gameCode, BuzzWord buzzword, boolean isWinner, String nickName, String winnerNickName) {
        this.gameCode = gameCode;
        this.buzzword = buzzword;
        this.isWinner = isWinner;
        this.nickName = nickName;
        this.winnerNickName = winnerNickName;
    }

    public String getGameCode() {
        return gameCode;
    }

    public BuzzWord getBuzzword() {
        return buzzword;
    }

    public boolean isWinner() {
        return isWinner;
    }

    public void setWinner(boolean winner) {
        isWinner = winner;
    }

    public String getNickName() {
        return nickName;
    }

    public String getWinnerNickName() {
        return winnerNickName;
    }

    public void setWinnerNickName(String winnerNickName) {
        this.winnerNickName = winnerNickName;
    }
}
