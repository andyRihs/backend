package com.hftm.transferprojekt.controller.model;

public class NewGameResponse {
    private final String gameCode;
    private final String gameName;

    public NewGameResponse(String gameCode, String gameName) {
        this.gameCode = gameCode;
        this.gameName = gameName;
    }

    public String getGameCode() {
        return gameCode;
    }

    public String getGameName() {
        return gameName;
    }
}
