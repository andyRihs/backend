package com.hftm.transferprojekt.controller.model;

public class StartGameRequest {
    private final String gameCode;

    public StartGameRequest(String gameCode) {
        this.gameCode = gameCode;
    }

    public String getGameCode() {
        return gameCode;
    }
}
