package com.hftm.transferprojekt.controller.model;

public class WinnerResultsAcceptedRequest {
    private final String gameCode;
    private final String nickName;

    public WinnerResultsAcceptedRequest(String gameCode, String gameName) {
        this.gameCode = gameCode;
        this.nickName = gameName;
    }

    public String getGameCode() {
        return gameCode;
    }

    public String getNickName() {
        return nickName;
    }
}
