package com.hftm.transferprojekt.controller.model;

public class JoinGameRequest {
    private final String gameCode;
    private final String nickName;

    public JoinGameRequest(String gameCode, String nickName) {
        this.gameCode = gameCode;
        this.nickName = nickName;
    }

    public String getGameCode() {
        return gameCode;
    }

    public String getNickName() {
        return nickName;
    }
}
