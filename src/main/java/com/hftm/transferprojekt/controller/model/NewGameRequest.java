package com.hftm.transferprojekt.controller.model;

import com.hftm.transferprojekt.model.Level;

import java.util.Date;


public class NewGameRequest {
    private final String gameName;
    private final String nickName;
    private final Date gameStart;
    private final int gameDuration;
    private final boolean needFullCard;
    private final Level gamelevel;

    public NewGameRequest(String gameName, String nickName, Date gameStart, int gameDuration, boolean needFullCard, Level gamelevel) {
        this.gameName = gameName;
        this.nickName = nickName;
        this.gameStart = gameStart;
        this.gameDuration = gameDuration;
        this.needFullCard = needFullCard;
        this.gamelevel = gamelevel;
    }

    public String getGameName() {
        return gameName;
    }

    public String getNickName() {
        return nickName;
    }

    public Date getGameStart() {
        return gameStart;
    }

    public int getGameDuration() {
        return gameDuration;
    }

    public boolean getNeedFullCard(){ return needFullCard; }

    public Level getGamelevel() {
        return gamelevel;
    }
}
