package com.hftm.transferprojekt.controller.model;

import com.hftm.transferprojekt.model.BuzzWord;
import java.util.List;

public class WinnerResultsResponse {
    private final String gameCode;
    private final String nickName;
    private final List<BuzzWord> buzzWords;

    public WinnerResultsResponse(String gameCode, String nickName, List<BuzzWord> buzzWords) {
        this.gameCode = gameCode;
        this.nickName = nickName;
        this.buzzWords = buzzWords;
    }

    public String getGameCode() {
        return gameCode;
    }

    public String getNickName() {
        return nickName;
    }

    public List<BuzzWord> getBuzzWords() {
        return buzzWords;
    }
}
