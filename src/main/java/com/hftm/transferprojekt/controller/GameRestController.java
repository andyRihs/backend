package com.hftm.transferprojekt.controller;

import com.hftm.transferprojekt.controller.model.*;
import com.hftm.transferprojekt.exceptions.*;
import com.hftm.transferprojekt.gamelogic.GameLogic;
import com.hftm.transferprojekt.model.BuzzWord;
import com.hftm.transferprojekt.model.Card;
import com.hftm.transferprojekt.model.Game;
import com.hftm.transferprojekt.model.Player;
import com.hftm.transferprojekt.persistence.IGameRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;

@RestController
@CrossOrigin(origins = "*")
public class GameRestController {
    private static Logger log = LoggerFactory.getLogger(GameRestController.class);

    @Autowired
    GameLogic gameLogic;

    @Async
    @PostMapping(path = "/newGame", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CompletableFuture<NewGameResponse> newGame(@RequestBody NewGameRequest newGameRequest) throws NewGameException {
        return CompletableFuture.supplyAsync(() -> {

            Callable<Game> newGame = () -> gameLogic.newGame(
                newGameRequest.getGameName(),
                newGameRequest.getNeedFullCard(),
                newGameRequest.getGameDuration(),
                newGameRequest.getGameStart(),
                newGameRequest.getNickName(),
                newGameRequest.getGamelevel());
            try {
                Game finalGame = newGame.call();
                return new NewGameResponse(
                    finalGame.getGameCode(),
                    finalGame.getGameName());
            } catch (Exception e) {
                throw new NewGameException(e);
            }
        });
    }

    @Async
    @PostMapping(path = "/joinGame", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CompletableFuture<JoinGameResponse> joinGame(@RequestBody JoinGameRequest joinGameRequest) throws JoinGameException {
        return CompletableFuture.supplyAsync(() -> {

            Callable<Game> joinGame = () -> gameLogic.joinGame(
                joinGameRequest.getGameCode(),
                joinGameRequest.getNickName());

            try {
                Game finalGame = joinGame.call();
                return new JoinGameResponse(
                    finalGame.getGameCode(),
                    finalGame.getGameName(),
                    finalGame.getStartDate(),
                    finalGame.getDuration(),
                    finalGame.getLevel());
            } catch (Exception e){
                throw new JoinGameException(e);
            }
        });
    }

    @PostMapping(path = "/createCard", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CreateCardResponse createCard(@RequestBody CreateCardRequest createCardRequest) throws CreateCardException {
        try{
            this.gameLogic.createCard(
                createCardRequest.getGameCode(),
                createCardRequest.getNickName(),
                createCardRequest.getCard());
        } catch (Exception e) {
            throw new CreateCardException(e);
        }

        return new CreateCardResponse(
            createCardRequest.getGameCode());
    }

    @PostMapping(path = "/buzzWordHit", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public BuzzWordHitResponse buzzWordHit(@RequestBody BuzzWordHitRequest buzzWordHitRequest) throws BuzzWordHitException {
        BuzzWord buzzWordToReturn = null;

        try{
            buzzWordToReturn = this.gameLogic.hitBuzzword(
                buzzWordHitRequest.getGameCode(),
                buzzWordHitRequest.getNickName(),
                buzzWordHitRequest.getBuzzword());
        } catch (Exception e) {
            throw new BuzzWordHitException("buzzWordToReturn failed", e);
        }

        if(buzzWordToReturn == null){
            buzzWordToReturn = buzzWordHitRequest.getBuzzword();
        }

        boolean isWinner;
        try {
            isWinner = this.gameLogic.playerHasWon(buzzWordHitRequest.getGameCode(), buzzWordHitRequest.getNickName());
        }catch (Exception e){
            throw new BuzzWordHitException("isWinner can not be determined", e);
        }

        String winnerNickName = null;
        try {
            winnerNickName = this.gameLogic.getWinnerNickName(buzzWordHitRequest.getGameCode());
        }catch (Exception e){
            throw new BuzzWordHitException("read Winner from Game not possible", e);
        }

        return new BuzzWordHitResponse(
            buzzWordHitRequest.getGameCode(),
            buzzWordToReturn,
            isWinner,
            buzzWordHitRequest.getNickName(),
            winnerNickName);
    }
}
