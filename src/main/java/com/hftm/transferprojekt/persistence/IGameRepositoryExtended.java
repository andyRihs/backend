package com.hftm.transferprojekt.persistence;

import com.hftm.transferprojekt.model.Game;
import java.util.List;

public interface IGameRepositoryExtended {
    List<Game> findGameByGameName(String gameName);
}
