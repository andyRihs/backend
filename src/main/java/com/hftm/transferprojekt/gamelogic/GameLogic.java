package com.hftm.transferprojekt.gamelogic;

import com.hftm.transferprojekt.exceptions.GameLogicInputException;
import com.hftm.transferprojekt.model.*;
import com.hftm.transferprojekt.persistence.IGameRepository;
import com.hftm.transferprojekt.persistence.IPlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
public class GameLogic implements IGameLogic {

    //Error messages
    private static final String MSG_GAMENAME_WRONG = "Gamename empty or null";
    private static final String MSG_DURATION_WRONG = "Game duration to small";
    private static final String MSG_STARTDATE_WRONG = "Startdate empty or in past";
    private static final String MSG_NICKNAME_WRONG = "Nickname empty or null";
    private static final String MSG_GAMECODE_WRONG = "Gamecode empty or null";
    private static final String MSG_GAME_NOTFOUND = "Gamecode wrong or game not found";
    private static final String MSG_NICKNAME_ALREADY_INUSE = "Nickname is already in use for this game";
    private static final String MSG_NICKNAME_NOT_INUSE = "Nickname do not exists for this game";
    private static final String MSG_CARD_IS_EMPTY = "Card is null or empty";
    private static final String MSG_BUZZWORD_IS_EMPTY = "BuzzWord is null or empty";
    private static final String MSG_BUZZWORD_IS_NOT_UNIQE = "BuzzWord is not not uniqe";
    private static final String MSG_LEVEL_NOT_FOUND = "Level not found";
    @Autowired
    private IGameRepository gameRepository;
    @Autowired
    private IPlayerRepository playerRepository;

    public static byte[] asByteArray(UUID uuid) {
        long msb = uuid.getMostSignificantBits();
        long lsb = uuid.getLeastSignificantBits();
        byte[] buffer = new byte[16];

        for (int i = 0; i < 8; i++) {
            buffer[i] = (byte) (msb >>> 8 * (7 - i));
        }

        for (int i = 8; i < 16; i++) {
            buffer[i] = (byte) (lsb >>> 8 * (7 - i));
        }

        return buffer;
    }

    @Override
    public Game newGame(String gameName, boolean needFullCard, int duration, Date startDate, String nickName, Level gameLevel) throws GameLogicInputException {
        if (gameName == null || gameName.equals("")) {
            throw new GameLogicInputException(MSG_GAMENAME_WRONG);
        }

        if (duration <= 0) {
            throw new GameLogicInputException(MSG_DURATION_WRONG);
        }

        if (startDate == null || startDate.before(new Date(System.currentTimeMillis()))) {
            throw new GameLogicInputException(MSG_STARTDATE_WRONG);
        }

        if (nickName == null || nickName.equals("")) {
            throw new GameLogicInputException(MSG_NICKNAME_WRONG);
        }

        //check gamename
        if (gameRepository.findGameByGameName(gameName) == null) { // Diese überprüfung ist gefährlich und verhindert, dass zwei Games gleich heissen dürfen.
            //create User
            Player gameMaster = new Player(nickName);
            playerRepository.save(gameMaster);

            Game game = new Game(duration, needFullCard, gameName, startDate, gameLevel, gameMaster);
            game.setGameCode(iGenerateGameCode());
            gameRepository.save(game);
            return game;
        }

        return null;
    }

    @Override
    public Game joinGame(String gameCode, String nickName) throws GameLogicInputException {
        if (gameCode == null || gameCode.equals("")) {
            throw new GameLogicInputException(MSG_GAMECODE_WRONG);
        }

        if (nickName == null || nickName.equals("")) {
            throw new GameLogicInputException(MSG_NICKNAME_WRONG);
        }

        Game game = gameRepository.findGameByGameCode(gameCode);
        if (game == null) {
            throw new GameLogicInputException(MSG_GAME_NOTFOUND);
        }

        if (iPlayerExists(game, nickName)) {
            throw new GameLogicInputException(MSG_NICKNAME_ALREADY_INUSE);
        }

        List<Player> players = game.getPlayers();
        Player player = new Player(nickName, game);
        playerRepository.save(player);
        players.add(player);
        gameRepository.save(game);
        return game;
    }

    @Override
    public void createCard(String gameCode, String nickName, Card card) throws GameLogicInputException {
        if (gameCode == null || gameCode.equals("")) {
            throw new GameLogicInputException(MSG_GAMECODE_WRONG);
        }

        if (nickName == null || nickName.equals("")) {
            throw new GameLogicInputException(MSG_NICKNAME_WRONG);
        }

        Game game = gameRepository.findGameByGameCode(gameCode);
        if (game == null) {
            throw new GameLogicInputException(MSG_GAME_NOTFOUND);
        }

        if (!iPlayerExists(game, nickName)) {
            throw new GameLogicInputException(MSG_NICKNAME_NOT_INUSE);
        }

        if (card == null || card.getBuzzWords().isEmpty()) {
            throw new GameLogicInputException(MSG_CARD_IS_EMPTY);
        }

        ListIterator<BuzzWord> buzzwordIterator = card.getBuzzWords().listIterator();
        ArrayList<String> buzzwordStringList = new ArrayList<>();
        while (buzzwordIterator.hasNext()) {
            buzzwordStringList.add(buzzwordIterator.next().getBuzzWord());
        }

        Collections.sort(buzzwordStringList);
        String lastBuzzword = null;
        for (String currentBuzzword : buzzwordStringList) {
            if (currentBuzzword.isEmpty()) {
                throw new GameLogicInputException(MSG_BUZZWORD_IS_EMPTY);
            }

            if (currentBuzzword.equals(lastBuzzword)) {
                throw new GameLogicInputException(MSG_BUZZWORD_IS_NOT_UNIQE);
            }

            lastBuzzword = currentBuzzword;
        }

        switch (game.getLevel()) {
            case EASY:
                if (card.getBuzzWords().size() != 9) {
                    throw new GameLogicInputException("Buzzword count in Card = " + card.getBuzzWords().size() + " should be 9 at EASY!");
                }

                break;
            case MEDIUM:
                if (card.getBuzzWords().size() != 16) {
                    throw new GameLogicInputException("Buzzword count in Card = " + card.getBuzzWords().size() + " should be 16 at MEDIUM!");
                }

                break;
            case HEAVY:
                if (card.getBuzzWords().size() != 25) {
                    throw new GameLogicInputException("Buzzword count in Card = " + card.getBuzzWords().size() + " should be 25 at HEAVY!");
                }

                break;
            default:
                throw new GameLogicInputException(MSG_LEVEL_NOT_FOUND);
        }

        for (Player player : game.getPlayers()) {
            if (player.getNickName().equals(nickName)) {
                if (player.getCard() != null) {
                    Card fromPlayer = player.getCard();
                    fromPlayer.setBuzzWords(card.getBuzzWords());
                } else {
                    player.setCard(card);
                }

                playerRepository.save(player);
            }
        }
    }

    @Override
    public BuzzWord hitBuzzword(String gameCode, String nickName, BuzzWord buzzWord) throws GameLogicInputException {
        if (gameCode == null || gameCode.equals("")) {
            throw new GameLogicInputException(MSG_GAMECODE_WRONG);
        }

        if (nickName == null || nickName.equals("")) {
            throw new GameLogicInputException(MSG_NICKNAME_WRONG);
        }

        Game game = gameRepository.findGameByGameCode(gameCode);
        if (game == null) {
            throw new GameLogicInputException(MSG_GAME_NOTFOUND);
        }

        if (!iPlayerExists(game, nickName)) {
            throw new GameLogicInputException(MSG_NICKNAME_NOT_INUSE);
        }

        if (buzzWord == null || buzzWord.getBuzzWord() == null) {
            throw new GameLogicInputException(MSG_BUZZWORD_IS_EMPTY);
        }

        List<Player> players = game.getPlayers();
        Player player = null;
        for (Player entry : players) {
            if (entry.getNickName().equals(nickName)) {
                player = entry;
            }
        }

        if (player == null){
            throw new GameLogicInputException(MSG_NICKNAME_WRONG);
        }

        Card card = player.getCard();

        if (card == null) {
            throw new GameLogicInputException(MSG_CARD_IS_EMPTY);
        }

        BuzzWord buzzWordFromCard = null;
        List<BuzzWord> buzzWords = card.getBuzzWords();
        for (BuzzWord entry : buzzWords) {
            if (entry.getBuzzWord().equals(buzzWord.getBuzzWord())) {
                buzzWordFromCard = entry;
            }
        }

        if (buzzWordFromCard == null) {
            throw new GameLogicInputException(MSG_BUZZWORD_IS_EMPTY);
        }

        int currentCount = buzzWordFromCard.getCounter();
        currentCount++;
        buzzWordFromCard.setCounter(currentCount);
        playerRepository.save(player);
        return buzzWordFromCard;
    }

    @Override
    public Player getPlayer(String gameCode, String nickName) {
        Game game = gameRepository.findGameByGameCode(gameCode);

        for (Player player: game.getPlayers()) {
            if (player.getNickName().equals(nickName)){
                return player;
            }
        }

        return null;
    }

    @Override
    public boolean playerHasWon(String gameCode, String nickName) throws GameLogicInputException {
        if (gameCode == null || gameCode.equals("")) {
            throw new GameLogicInputException(MSG_GAMECODE_WRONG);
        }

        if (nickName == null || nickName.equals("")) {
            throw new GameLogicInputException(MSG_NICKNAME_WRONG);
        }

        Game game = gameRepository.findGameByGameCode(gameCode);
        if (game == null) {
            throw new GameLogicInputException(MSG_GAME_NOTFOUND);
        }

        Player player = getPlayer(gameCode, nickName);

        if (player == null){
            throw new GameLogicInputException(MSG_NICKNAME_NOT_INUSE);
        }

        boolean isWinner = false;

        if (game.isNeedFullCard()) {
            int checkIsWiner = 0;
            for (BuzzWord buzzWord : player.getCard().getBuzzWords()) {
                if (buzzWord.getCounter() == 0) {
                    checkIsWiner = +1;
                    break;
                }
            }

            if (checkIsWiner == 0) {
                isWinner = true;
                setWinner(gameCode, nickName);
            }

        } else {
            for (int i = 0; i < Math.sqrt(player.getCard().getBuzzWords().size()); i++) {
                int checkIsWinnerX = 0;
                int checkIsWinnerY = 0;
                for (BuzzWord buzzWord : player.getCard().getBuzzWords()) {
                    if (buzzWord.getxPosition() == i) {
                        if (buzzWord.getCounter() == 0) {
                            checkIsWinnerX = +1;
                        }
                    }

                    if (buzzWord.getyPosition() == i) {
                        if (buzzWord.getCounter() == 0) {
                            checkIsWinnerY = +1;
                        }
                    }
                }

                if (checkIsWinnerX == 0 || checkIsWinnerY == 0) {
                    isWinner = true;
                    setWinner(gameCode, nickName);
                }
            }
        }

        return isWinner;
    }

    @Override
    public void setWinner(String gameCode, String nickName) {
        Game game = gameRepository.findGameByGameCode(gameCode);
        game.setWinnerNickName(nickName);
        gameRepository.save(game);
    }

    public String getWinnerNickName (String gameCode){
        return gameRepository.findGameByGameCode(gameCode).getWinnerNickName();
    }

    private String iGenerateGameCode() {
        boolean notNewGameCode = true;
        String gameCode = "";
        while (notNewGameCode) {
            UUID uuid = UUID.randomUUID();
            byte[] uuidArr = asByteArray(uuid);
            gameCode = Base64.getEncoder().encodeToString(uuidArr).replaceAll("=+$", "");
            notNewGameCode = gameRepository.findGameByGameCode(gameCode) != null;
        }

        return gameCode;
    }

    private Boolean iPlayerExists(Game game, String nickName) {
        return game.getPlayers()
                .stream()
                .anyMatch(t ->
                        t.getNickName()
                                .equals(nickName));
    }
}
