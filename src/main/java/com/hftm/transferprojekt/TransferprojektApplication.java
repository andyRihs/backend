package com.hftm.transferprojekt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.hftm.transferprojekt.persistence")
@ComponentScan({"com.hftm.transferprojekt.gamelogic","com.hftm.transferprojekt.controller", "com.hftm.transferprojekt.persistence","com.hftm.transferprojekt.config"})
public class TransferprojektApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransferprojektApplication.class, args);
	}

}
